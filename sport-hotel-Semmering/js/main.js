var s, Site = {
    settings: {
        $body: $("body"),
        $onlineBooking: $(".online-booking"),
        $openForm: $("#open-form"),
        $clodeForm: $("#close-form"),
        $banner: $(".banner"),
    },
    init: function() {
        s = this.settings;
        this._bindActions();
//        this._bind_actions();
        this.run();
    },
    
    _bindActions: function() {
        s.$banner.removeClass("active");
        s.$openForm.on('click', function() {
            $(this).hide();
            s.$banner.addClass("active");
            s.$onlineBooking.slideDown(300, function() {
                s.$clodeForm.show();
            });
        });
        s.$clodeForm.on('click', function() {
            $(this).hide();
            s.$onlineBooking.slideUp(300, function() {
                s.$openForm.show();
                s.$banner.removeClass("active");
            });
        });
    },
    
    run: function() {
        // calendar plugin
        $('.race').datepicker({
            language: 'ua',
            format:"dd.mm.yyyy",
            weekStart: 1,
            autoclose: true
        })
        .on('changeDate', function(selected){
            var startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('.departure').datepicker('setStartDate', startDate);
        }); 
        $('.departure').datepicker({
            format:"dd.mm.yyyy",
            weekStart: 0,
            language: 'ua',
            autoclose: true
        })
        .on('changeDate', function(selected){
            var FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('.race').datepicker('setEndDate', FromEndDate);
        });

        $('.hotel-foto').magnificPopup({
            mainClass: 'mfp-with-zoom',
            easing: 'ease-in-out',
            gallery :{
                enabled: true,
            },      
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image'
            // other options
        });
    }
    
}

$.extend({
    Site: Site,
    s: s
});

$(document).ready(function () {
    Site.init();
});